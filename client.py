import websocket
import threading
import json


def on_message(wsapp: websocket.WebSocketApp, message):
	print(message)

def main():
	wsapp = websocket.WebSocketApp("ws://localhost:8080", on_message=on_message)
	# Start websocket event loop on another thread
	wsThread = threading.Thread(target=wsapp.run_forever)
	wsThread.start()
	try:
		# Scan for inputs
		while True:
			inp = input()
			args = inp.split(" ")
			command = args[0]
			if command == "send":
				wsapp.send(args[1])
	except:
		# On exception close the socket and thread
		wsapp.close()
		wsThread.join()


if __name__ == "__main__":
	main()
